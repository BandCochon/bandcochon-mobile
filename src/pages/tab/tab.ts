import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { PicturePage } from '../picture/picture';
import { CameraPage } from '../camera/camera';
import { SettingsPage } from '../settings/settings';
import { SettingsService } from '../../services/settings';

@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html'
})
export class TabPage {
  pictures = PicturePage;
  camera = CameraPage;
  settings = SettingsPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, private settingsService: SettingsService) {}  
}
