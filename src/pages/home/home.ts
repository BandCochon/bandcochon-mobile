import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';

import { NavController, AlertController, LoadingController } from 'ionic-angular';

import { RegisterPage } from '../register/register';
import { TabPage } from '../tab/tab';

import { BandCochonService, LoginResult } from '../../services/bandcochonservice';

/*
TODO:
    * check if the network is present

 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  email: string = '';
  password: string = '';
  displayConnectForm = false;
  networkError = false;

  constructor(private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private bandcochonService: BandCochonService) { }

  ngOnInit(): void {
    this.tryToPing();
  }

  private tryToPing(): void {
    this.bandcochonService
      .ping()
      .subscribe(
      (connected: boolean) => {
        this.navCtrl.setRoot(TabPage);
      },
      (err: Response) => {
        this.networkError = err.status === 0;
        this.displayConnectForm = !this.networkError;

        if (this.networkError === true) {
          this.displayPingErrorDialog();
        } 
        
      });
  }

  connect(): void {
    this.email = this.email.trim();
    this.password = this.password.trim();

    if (this.email === '' || this.password === '') {
      let missing = this._determineLoginFormError();

      this.alertCtrl.create({
        title: "Erreur",
        subTitle: `Il manque ${missing}.`,
        buttons: ['OK'],
      }).present();
      return;
    }

    const loader = this.loadingCtrl.create({ content: 'Connexion à Band Cochon' });
    loader.present();

    this.bandcochonService
      .login({ email: this.email, password: this.password })
      .subscribe(
      (result: LoginResult) => {
        this.navCtrl.setRoot(TabPage);
      },

      (error: string) => {
        let alert = this.alertCtrl.create({
          title: "Erreur d'autentification",
          subTitle: "Impossible de vous connecter. Êtes-vous sûr d'avoir entré les bons identiants ?",
          buttons: ['OK']
        });
        alert.present();
      }).add(() => {
        loader.dismiss();
      });
  }

  register(): void {
    this.navCtrl.push(RegisterPage);
  }

  enterCode(): void {
    alert('à faire');
  }

  private _determineLoginFormError() {
    let missing = [];
    if (this.email.length === 0) {
      missing.push('votre adresse email');
    }

    if (this.password.length === 0) {
      missing.push('votre mot de passe');
    }

    return missing.join(' et ');
  }

  private displayPingErrorDialog(): void {
    this.alertCtrl.create({
      title: "Erreur",
      message: "Impossible de contacter le serveur. Êtes-vous sûr d'avoir activé l'accès à Internet&nbsp;?",
      buttons: ['OK']
    }).present();
  }
}
