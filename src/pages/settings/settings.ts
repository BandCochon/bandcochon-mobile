import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, App } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs';

import { SettingsService } from '../../services/settings';
import { BandCochonService } from '../../services/bandcochonservice';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage implements OnInit {
  _onlyWifi = new BehaviorSubject<boolean>(false);
  _forceGPS = new BehaviorSubject<boolean>(false);

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public app: App,
    private alertCtrl: AlertController,
    private settingsService: SettingsService,
    private bandcochonService: BandCochonService) {
  }

  ngOnInit(): void {
    this._forceGPS.next(this.settingsService.forceGPS);
    this._onlyWifi.next(this.settingsService.onlyWifi);
  }

  changeWifi() {
    let val = !this._onlyWifi.getValue();
    this._onlyWifi.next(val);
    this.settingsService.onlyWifi = val;
  }

  changeGps() {
    let val = !this._forceGPS.getValue();
    this._forceGPS.next(val);
    this.settingsService.forceGps = val;
  }

  get onlyWifi(): boolean {
    return this._onlyWifi.getValue();
  }

  get forceGPS(): boolean {
    return this._forceGPS.getValue();
  }

  disconnect(): void {
    this.bandcochonService.logout().subscribe(() => {
      this.app.getRootNav().setRoot(HomePage);
    }, (err) => {
      let alert = this.alertCtrl.create({
        title: "Hum… C'est embarrasant.",
        subTitle: "Il semble qu'il est impossible de vous déconnecter pour le moment. <br>Merci de ré-essayer plus tard.",
        buttons: ['OK']
      });
      alert.present();
    });
  }
}
