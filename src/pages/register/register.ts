import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { Response } from '@angular/http';

import { BandCochonService } from '../../services/bandcochonservice';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  username: string = ''
  email: string = '';
  password: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private authService: BandCochonService) { }

  createAccount(): void {
    if (this.username.trim().length === 0) {
      this.displayMissingUsernameDialog();
    }

    else if (this.username.length < 4) {
      this.displayUsernameTooShortDialog();
    }

    else if (this.email.trim().length === 0) {
      this.displayMissingEmailDialog();
    }

    else if (!/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(this.email)) {
      this.displayInvalidMailAddressDialog();
    }

    else if (this.password.trim().length === 0) {
      this.displayMissingPasswordDialog();
    }

    else if (this.password.length < 6) {
      this.displayPasswordTooShortDialog();
    }

    else {
      this.authService.register(this.username, this.email, this.password)
        .subscribe(
        (response: Response) => {
          this.displaySuccessDialog().then(() => {
            this.navCtrl.pop();
          });
        },
        (err: Response) => {
          this.displayErrorMessageFromServer(err);
        });
    }

  }

  connectWithGoogle(): void {
    alert('à faire');
  }

  connectWithFacebook(): void {
    alert('à faire');
  }

  private displayMissingUsernameDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "Le nom de l'utilisateur doit être défini.",
      message: "Vous n'avez pas défini de nom d'utilisateur ou celui-ci n'est que d'un espace.",
      buttons: ['OK'],
    }).present();
  }

  private displayUsernameTooShortDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "Le nom d'utilisateur trop court.",
      message: "Afin d'éviter toute confusion, il est préférable d'avoir un nom d'utilisateur de plus de 6 caractères.",
      buttons: ['OK'],
    }).present();
  }

  private displayMissingEmailDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "L'adresse email n'a pas été entrée.",
      message: "Vous n'avez pas entrer d'adresse email",
      buttons: ['OK'],
    }).present();
  }

  private displayMissingPasswordDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "Le mot de passe est absent.",
      message: "Il est impossible de ne pas avoir de mot de passe.",
      buttons: ['OK'],
    }).present();
  }

  private displayPasswordTooShortDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "Le mot de passe est trop court.",
      message: "Afin d'augmenter la sécurité de nos utilisateurs, nous vous demandons un mot de passe d'au moins 6 caractères.",
      buttons: ['OK'],
    }).present();
  }

  private displayInvalidMailAddressDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Erreur",
      subTitle: "L'adresse email n'est pas valide.",
      message: "L'adresse email que vous avez entrée n'est pas correcte. Il est impossible de s'en servir.",
      buttons: ['OK'],
    }).present();
  }

  private displayErrorMessageFromServer(err: Response): void {
    this.alertCtrl.create({
      title: "Erreur",
      subTitle: "Une erreur est survenue",
      message: err.text(),
      buttons: ['OK']
    }).present();
  }

  private displaySuccessDialog(): Promise<any> {
    return this.alertCtrl.create({
      title: "Succès",
      subTitle: "L'enregistrement s'est bien déroulé.",
      message: "Nous venons de vous envoyer un email de confirmation. Merci de suivre le lien de cet email. Vous pourrez ensuite vous connecter avec l'application",
      buttons: ['OK']
    }).present();
  }
}
