import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
//import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { IBandCochon } from './ibandcochon';

export interface Preferences {
    forceGPS: boolean;
    onlyWifi: boolean;
}

@Injectable()
export class SettingsService extends IBandCochon {
    preferences: Preferences = { forceGPS: false, onlyWifi: false };

    constructor(private httpClient: Http) {
        super();
        this.loadPreferences();
    }


    saveSettings(): void {
        this.httpClient
            .put(this.constructUrl('settings'), { 'forceGPS': this.preferences.forceGPS, 'onlyWifi': this.preferences.onlyWifi }, this.generateRequestOptionsArgs())
            .subscribe((response: Response) => { this.preferences = response.json() });
    }

    loadPreferences() {
        return this.httpClient
            .get(this.constructUrl('settings'), this.generateRequestOptionsArgs())
            .subscribe((res: Response) => { this.preferences = res.json() });
    }

    set onlyWifi(val: boolean) {
        this.preferences.onlyWifi = val;
        this.saveSettings();
    }

    get onlyWifi(): boolean {
        return this.preferences.onlyWifi;
    }

    get forceGPS(): boolean {
        return this.preferences.forceGPS;
    }

    set forceGps(val: boolean) {
        this.preferences.forceGPS = val;
        this.saveSettings();
    }
}