import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { IBandCochon } from './ibandcochon';

export interface LoginType {
    email: string;
    password: string;
}

export interface LoginResult {
    token: string;
}

@Injectable()
export class BandCochonService extends IBandCochon {

    constructor(private httpClient: Http) {
        super();
    }

    ping(): Observable<boolean> {
        const params = this.token !== null ? { token: this.token } : null;
        return this.httpClient.post(this.constructUrl('ping'), params, this.generateRequestOptionsArgs())
            .catch((err: Response) => {
                return Observable.throw(err);
            });
    }   

    login(loginInfo: LoginType): Observable<LoginResult> {
        return this.httpClient.post(this.constructUrl('login'), loginInfo, this.generateRequestOptionsArgs())
            .map((res: Response) => {
                let loginResult: LoginResult = res.json();
                this.saveToken(loginResult.token);
                return res.json();
            }).catch((err: Response) => Observable.throw(err));
    }


    logout(): Observable<void> {
        return this.httpClient.post(this.constructUrl('logout'), {}, this.generateRequestOptionsArgs())
            .map((response: Response) => { this.clearToken() })
            .catch((err: Response) => Observable.throw(err))
    }

    register(username: string, email: string, password: string): Observable<Response> {
        return this.httpClient.post(this.constructUrl('register'), {username, email, password}, this.generateRequestOptionsArgs());
    }
}
