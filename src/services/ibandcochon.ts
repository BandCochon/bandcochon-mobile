import { Headers, ResponseOptionsArgs, ResponseType } from '@angular/http';

export const AUTH_TOKEN = 'X-AUTH-TOKEN';

export class IBandCochon {
    protected headers: Headers = new Headers();
    protected token: string;

    constructor() {
        this.loadToken();
    }

    protected constructUrl(url: string): string {
        return `http://localhost:8000/api/${url}/`;
        //        return `http://www.bandcochon.re/api/${url}/`;
    }

    protected setTokenToHeader() {
        if (this.token) {
            this.headers.set(AUTH_TOKEN, this.token);
        } else {
            this.removeTokenFromHeader();
        }
    }

    protected removeTokenFromHeader() {
        this.headers.delete(AUTH_TOKEN);
    }

    protected saveToken(token: string): void {
        this.token = token;
        window.localStorage.setItem('bandcochon-token', token);
        this.setTokenToHeader();
    }

    protected loadToken(): void {
        this.token = window.localStorage.getItem('bandcochon-token');
        this.setTokenToHeader();
    }

    protected clearToken(): void {
        this.token = null;
        this.removeTokenFromHeader();
        window.localStorage.removeItem('bandcochon-token');
    }

    protected generateRequestOptionsArgs(args?: any): ResponseOptionsArgs {
        return {
            body: args,
            headers: this.headers,
            type: ResponseType.Cors,
        }
    }
}