import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { TabPage } from '../pages/tab/tab';
import { PicturePage } from '../pages/picture/picture';
import { CameraPage } from '../pages/camera/camera';
import { SettingsPage } from '../pages/settings/settings';

import { BandCochonService } from '../services/bandcochonservice';
import { SettingsService } from '../services/settings';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    TabPage,
    PicturePage,
    CameraPage, 
    SettingsPage,
  ],
  
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  
  bootstrap: [IonicApp],

  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    TabPage,
    PicturePage,
    CameraPage, 
    SettingsPage,
  ],
  
  providers: [
    {
      provide: ErrorHandler, useClass: IonicErrorHandler
    },

    BandCochonService,
    SettingsService,    
  ]
})
export class AppModule {}
